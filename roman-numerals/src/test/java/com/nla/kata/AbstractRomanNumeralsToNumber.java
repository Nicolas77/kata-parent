package com.nla.kata;

import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractRomanNumeralsToNumber {

    public abstract RomanNumerals createRomanNumerals();

    @Test
    public void nombre_2751() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("MMDCCLI");
        Assert.assertEquals(2751, actual);
    }

    @Test
    public void nombre_1000() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("M");
        Assert.assertEquals(1000, actual);
    }

    @Test
    public void nombre_400() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("CD");
        Assert.assertEquals(400, actual);
    }

    @Test
    public void nombre_200() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("CC");
        Assert.assertEquals(200, actual);
    }

    @Test
    public void nombre_600() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("DC");
        Assert.assertEquals(600, actual);
    }

    @Test
    public void nombre_448() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("CDXLVIII");
        Assert.assertEquals(448, actual);
    }

    @Test
    public void nombre_369() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("CCCLXIX");
        Assert.assertEquals(369, actual);
    }

    @Test
    public void nombre_cinquante() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("L");
        Assert.assertEquals(50, actual);
    }

    @Test
    public void nombre_dix() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("X");
        Assert.assertEquals(10, actual);
    }

    @Test
    public void nombre_neuf() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("IX");
        Assert.assertEquals(9, actual);
    }

    @Test
    public void nombre_cinq() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("V");
        Assert.assertEquals(5, actual);
    }

    @Test
    public void nombre_quatre() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("IV");
        Assert.assertEquals(4, actual);
    }

    @Test
    public void nombre_un() {
        RomanNumerals romanNumerals = createRomanNumerals();
        int actual = romanNumerals.convertFromRomanNumerals("I");
        Assert.assertEquals(1, actual);
    }
}
