package com.nla.kata;

public class RomanNumeralsWithEnumToNumberTest extends AbstractRomanNumeralsToNumber {

    @Override
    public RomanNumerals createRomanNumerals() {
        return new RomanNumeralsWithEnum();
    }

}
