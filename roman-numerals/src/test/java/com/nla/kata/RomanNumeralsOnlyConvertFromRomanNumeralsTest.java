package com.nla.kata;

public class RomanNumeralsOnlyConvertFromRomanNumeralsTest extends AbstractRomanNumeralsToNumber {

    @Override
    public RomanNumerals createRomanNumerals() {
        return new RomanNumeralsOnlyConvertFromRomanNumerals();
    }

}
