package com.nla.kata;

import org.junit.Assert;
import org.junit.Test;

public class NumberToRomanNumeralsWithEnumTest {

    @Test(expected = IllegalArgumentException.class)
    public void nombre_inferieur_a_5000() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        romanNumerals.convertToRomanNumerals(5000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nombre_superieur_a_0() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        romanNumerals.convertToRomanNumerals(0);
    }

    @Test
    public void nombre_2751() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(2751);
        Assert.assertEquals("MMDCCLI", actual);
    }

    @Test
    public void nombre_1000() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(1000);
        Assert.assertEquals("M", actual);
    }

    @Test
    public void nombre_400() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(400);
        Assert.assertEquals("CD", actual);
    }

    @Test
    public void nombre_200() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(200);
        Assert.assertEquals("CC", actual);
    }

    @Test
    public void nombre_600() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(600);
        Assert.assertEquals("DC", actual);
    }

    @Test
    public void nombre_448() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(448);
        Assert.assertEquals("CDXLVIII", actual);
    }

    @Test
    public void nombre_369() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(369);
        Assert.assertEquals("CCCLXIX", actual);
    }

    @Test
    public void nombre_cinquante() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(50);
        Assert.assertEquals("L", actual);
    }

    @Test
    public void nombre_dix() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(10);
        Assert.assertEquals("X", actual);
    }

    @Test
    public void nombre_neuf() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(9);
        Assert.assertEquals("IX", actual);
    }

    @Test
    public void nombre_cinq() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(5);
        Assert.assertEquals("V", actual);
    }

    @Test
    public void nombre_quatre() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(4);
        Assert.assertEquals("IV", actual);
    }

    @Test
    public void nombre_un() {
        RomanNumeralsWithEnum romanNumerals = new RomanNumeralsWithEnum();
        String actual = romanNumerals.convertToRomanNumerals(1);
        Assert.assertEquals("I", actual);
    }

}
