package com.nla.kata;

public interface RomanNumerals {
    String convertToRomanNumerals(int number);

    int convertFromRomanNumerals(String numeralNumber);
}
