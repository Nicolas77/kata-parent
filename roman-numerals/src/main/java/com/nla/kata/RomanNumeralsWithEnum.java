package com.nla.kata;

import java.util.Optional;
import java.util.stream.IntStream;

import static com.nla.kata.RomanNumeralsWithEnum.RomanNumeralsEnum.*;

/**
 * I means 1
 * V means 5  , IV -> 4
 * X -> 10 , IX -> 9, XXIV -> 24
 * L -> 50 , XL -> 40 , LXXX -> 80
 * C -> 100 , XC -> 90, CCCLXIX -> 369
 * D -> 500 , CD -> 400 , CDXLVIII -> 448
 * MILLIER -> 10000  , MMDCCLI -> 2751
 */
public class RomanNumeralsWithEnum implements RomanNumerals {

    enum RomanNumeralsEnum {
        UN("I", 1),
        CINQ("V", 5, UN),
        DIX("X", 10, UN),
        CINQUANTE("L", 50, DIX),
        CENT("C", 100, DIX),
        CINQ_CENTS("D", 500, CENT),
        MILLE("M", 1000, CENT);

        String roman;
        int value;
        Optional<RomanNumeralsEnum> minusPossibility;


        RomanNumeralsEnum(String roman, int value) {
            this.roman = roman;
            this.value = value;
            this.minusPossibility = Optional.empty();
        }

        RomanNumeralsEnum(String roman, int value, RomanNumeralsEnum minusPossibility) {
            this.roman = roman;
            this.value = value;
            this.minusPossibility = Optional.of(minusPossibility);
        }

        public String getRoman() {
            return roman;
        }

        public int getValue() {
            return value;
        }

        public boolean isMinusPossibilityOf(RomanNumeralsEnum romanNumeralsEnum) {
            if(this.minusPossibility.isPresent()) {
                return this.minusPossibility.get().equals(romanNumeralsEnum);
            }
            return false;
        }

        public static RomanNumeralsEnum getFromRoman(String val) {
            for(RomanNumeralsEnum romanNumeralsEnum : RomanNumeralsEnum.values()) {
                if(romanNumeralsEnum.getRoman().equals(val)) {
                    return romanNumeralsEnum;
                }
            }
            throw new IllegalArgumentException("Valeur inconnu: " + val);
        }
    }

    @Override
    public String convertToRomanNumerals(int number) {
        if (number >= 5000 || number < 1) {
            throw new IllegalArgumentException("Le chiffre doit être inférieur à 5000 et supérieur à 0");
        }
        StringBuilder response = new StringBuilder("");
        generateMillier(response, number / 1000);
        generateCentaine(response, (number / 100) % 10);
        generateDizaine(response, (number / 10) % 10);
        generateUnite(response, (number % 10));
        return response.toString();
    }

    @Override
    public int convertFromRomanNumerals(String numeralNumber) {
        if ("".equals(numeralNumber)) {
            throw new IllegalArgumentException("numeralNumber ne peut être vide ou null");
        }
        int result = 0;
        String[] array = numeralNumber.split("");
        for (int i = 0; i < array.length; i++) {
            RomanNumeralsEnum romanNumeralsEnum = RomanNumeralsEnum.getFromRoman(array[i]);
            int currentVal = romanNumeralsEnum.getValue();
            if(i + 1 < array.length) {
                RomanNumeralsEnum nextRomanNumeralsEnum = RomanNumeralsEnum.getFromRoman(array[i+1]);
                if(nextRomanNumeralsEnum.isMinusPossibilityOf(romanNumeralsEnum)) {
                    currentVal *= -1;
                }
            }
            result += currentVal;
        }
        return result;
    }

    private void generateMillier(StringBuilder response, Integer millier) {
        IntStream.range(0, millier).forEach(i -> response.append(MILLE.getRoman()));
    }

    private void generateCentaine(StringBuilder response, Integer value) {
        generate(response, value, CENT, CINQ_CENTS, MILLE);
    }

    private void generateDizaine(StringBuilder response, Integer value) {
        generate(response, value, DIX, CINQUANTE, CENT);
    }

    private void generateUnite(StringBuilder response, Integer value) {
        generate(response, value, UN, CINQ, DIX);
    }

    private void generate(StringBuilder response, Integer value, RomanNumeralsEnum low, RomanNumeralsEnum middle, RomanNumeralsEnum high) {
        if (value == 9) {
            response.append(low.getRoman());
            response.append(high.getRoman());
        } else {
            if (value >= 5) {
                response.append(middle.getRoman());
                IntStream.range(0, value - 5).forEach(i -> response.append(low.getRoman()));
            } else if (value == 4) {
                response.append(low.getRoman());
                response.append(middle.getRoman());
            } else {
                IntStream.range(0, value).forEach(i -> response.append(low.getRoman()));
            }
        }
    }


}
