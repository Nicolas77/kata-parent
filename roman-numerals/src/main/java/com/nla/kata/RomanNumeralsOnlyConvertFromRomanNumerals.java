package com.nla.kata;

import java.util.Map;

public class RomanNumeralsOnlyConvertFromRomanNumerals implements RomanNumerals{

    private Map<String, Integer> valuesMap = Map.of("I", 1,
            "V", 5,
            "X",10,
            "L", 50,
            "C", 100,
            "D", 500,
            "M", 1000);

    @Override
    public String convertToRomanNumerals(int number) {
        return null;
    }

    @Override
    public int convertFromRomanNumerals(String numeralNumber) {
        int res = 0;
        String[] split = numeralNumber.split("(?!^)");
        int max = split.length - 1;
        for(int i = max; i >= 0; i--) {
            Integer value = valuesMap.get(split[i]);
            if(i == max || value >= valuesMap.get(split[i+1])) {
                res += value;
            } else {
                res -= value;
            }
        }
        return res;
    }
}
