import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Created by nico on 15/12/2016.
 */
public class StringCalculator {

    public static int add(String numberString) {
        int sum = 0;
        List<String> errors = new ArrayList();
        String separator = ",";
        boolean newSeparator = isNewSeparator(numberString);
        if (newSeparator) {

            numberString = numberString.substring(2);
            if (numberString.startsWith("[") && numberString.contains("]")) {
                separator = numberString.substring(1, numberString.indexOf("]"));
                numberString = numberString.substring(separator.length() + 2);
            } else {
                separator = numberString.substring(0, 1);
                numberString = numberString.substring(separator.length());
            }
        }
//        String[] splitSeveralLines = numberString.split("\n");
//        for (String line : splitSeveralLines) {
//            sum = sumWithSeparator(line, sum, separator, errors);
//        }
        numberString = numberString.replaceAll(Pattern.quote("\n"), separator);
        numberString = numberString.replaceAll(Pattern.quote("\\n"), separator);
        sum = sumWithSeparator(numberString, sum, separator, errors);

        if (!errors.isEmpty()) {
            String error = errors.stream().collect(Collectors.joining(","));
            throw new IllegalArgumentException("negatives not allowed : " + error);
        }

        return sum;
    }

    private static boolean isNewSeparator(String numberString) {
        return numberString.startsWith("//");
    }

    private static int sumWithSeparator(String numberString, int sum, String separator, List errors) {
        String[] numbers = numberString.split(Pattern.quote(separator));
        for (String n : numbers) {
            if (!"".equals(n)) {
                try {
                    int number = Integer.parseInt(n);
                    if (number < 0) {
                        errors.add(n);
                    } else if (number < 1000) {
                        sum += number;
                    }
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Not a numberString");
                }
            }
        }
        return sum;
    }
}
