import org.junit.Test;

import static org.junit.Assert.*;

public class StringCalculatorTest {

    @Test
    public void test_empty_string_should_return_0() {
        assertEquals(0, StringCalculator.add(""));
    }

    @Test
    public void test_one_number() {
        assertEquals(2, StringCalculator.add("2"));
    }

    @Test
    public void test_two_number() {
        assertEquals(7, StringCalculator.add("2,5"));
    }
    @Test
    public void test_two_big_numbers() {
        assertEquals(71, StringCalculator.add("20,51"));
    }

    @Test
    public void test_five_number() {
        assertEquals(25, StringCalculator.add("2,5,3,8,7"));
    }

    @Test
    public void test_new_line() {
        assertEquals(6, StringCalculator.add("1\n2,3"));
    }

    @Test
    public void test_new_separator() {
        assertEquals(3, StringCalculator.add("//;\n1;2"));
    }

    @Test()
    public void test_negative_number() {
        try {
            StringCalculator.add("//;\n1;2;-6");
            fail();
        } catch (Exception e) {
            assertEquals("negatives not allowed : -6", e.getMessage());
        }
    }
    @Test()
    public void test_negative_numbers() {
        try {
            StringCalculator.add("//;\n1;2;-6;-9");
            fail();
        } catch (Exception e) {
            assertEquals("negatives not allowed : -6,-9", e.getMessage());
        }
    }

//    Delimiters can be of any length with the following format:  “//[delimiter]\n” for example: “//[***]\n1***2***3” should return 6
//    Allow multiple delimiters like this:  “//[delim1][delim2]\n” for example “//[*][%]\n1*2%3” should return 6.
//    make sure you can also handle multiple delimiters with length longer than one char

    @Test
    public void test_numbers_bigger_than_1000_should_be_ignored() {
        assertEquals(2, StringCalculator.add("1001,2"));
    }
    @Test
    public void test_big_delimiter() {
        assertEquals(6, StringCalculator.add("//[***]\\n1***2***3"));
    }

//    @Test
//    public void test()

}